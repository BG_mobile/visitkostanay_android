package kz.bgpro.www.visitkostanai;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler handler = new Handler();
        handler.postDelayed(runn1,2000);
    }

    Runnable runn1 = new Runnable() {
        @Override
        public void run() {

                startActivity(new Intent(MainActivity.this,WebActivity.class));
                finish();

        }
    };
}
